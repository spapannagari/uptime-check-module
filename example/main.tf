module uptime_check_deployment {

    source = "git::https://gitlab.com/spapannagari/uptime-check-module//module"
    project = var.project
    region = var.region
    env = var.env
    uptime_check_name = "Test Uptime Check"   
    uptime_check_timeout = "60s"
    uptime_check_period = "60s"
    uptime_check_url = "/health_check"
    uptime_check_port = "443"
    uptime_check_request_method = "GET"
    channel_name = "Multisource3 Alerts"
    uptime_check_host = "multisource3-api.property-ds.dev.gcp.ent.af1platform.com"
    alert_policy_name = "Test Alert Policy"
    condition_name = "Uptime Check URL - Health Check failed"
    alignment_period = "60s"
    per_series_aligner = "ALIGN_NEXT_OLDER"
    cross_series_reducer = "REDUCE_COUNT_FALSE"
    comparison = "COMPARISON_GT"
    duration = "60s"
    trigger_count = 5
    threshold_value = 0
    combiner = "OR"
}
