variable "project" {
  type        = string
  description = "Project name in GCP that the Cloud Run service will be deployed to."
}

variable "region" {
  type        = string
  description = "Region in GCP that is a valid destination for the Cloud Run service"
}

variable "env" {
  type        = string
  description = "Region in GCP that is a valid destination for the Cloud Run service"
}
