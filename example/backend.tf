terraform {
  backend "gcs" {
    bucket = "property-ds-dev-tfstate"
    prefix = "data-engineering-infrastructure/gcp/uptime-checks"
  }
 
  required_providers {
    google = {
      source = "hashicorp/google"
      version = ">= 4.32.0"
    }
  }
}
