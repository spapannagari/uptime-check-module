resource "google_monitoring_uptime_check_config" "example-uptime-check" {
  display_name = var.uptime_check_name
  timeout = var.uptime_check_timeout
  period = var.uptime_check_period
  selected_regions = ["USA"]

  http_check {
    use_ssl = true
    path = var.uptime_check_url
    port = var.uptime_check_port
    request_method = var.uptime_check_request_method
    headers = {
      Service = "UpTimeCheck"
    }
  }

  monitored_resource {
    type = "uptime_url"
    labels = {
      host = var.uptime_check_host
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

data "google_monitoring_notification_channel" "slack_channel" {
  display_name = var.channel_name
}

resource "google_monitoring_alert_policy" "example_alert_policy" {
  display_name = var.alert_policy_name
  count = var.env != "prod" ? 0: 1 # Create alert policy only in production
  conditions {
      display_name = var.condition_name
      condition_threshold {
      filter = "resource.type = \"uptime_url\" AND metric.type = \"monitoring.googleapis.com/uptime_check/check_passed\" AND metric.labels.check_id = \"${basename(google_monitoring_uptime_check_config.example-uptime-check.id)}\""
        aggregations {
            alignment_period = var.alignment_period
            per_series_aligner = var.per_series_aligner
            cross_series_reducer = var.cross_series_reducer
          }
        comparison = var.comparison
        duration = var.duration
        trigger {
          count = var.trigger_count
        }
        threshold_value = var.threshold_value
      }
    }
  combiner = var.combiner
  enabled = true
  notification_channels = [
    data.google_monitoring_notification_channel.slack_channel.name
  ]
}