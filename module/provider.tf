terraform {

  required_providers {
    google-beta = {
      source = "hashicorp/google-beta"
      version = ">= 4.32.0"
    }
    google = {
      source = "hashicorp/google"
      version = ">= 4.32.0"
    }
  }
}

