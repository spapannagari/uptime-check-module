variable "project" {
    type = string
    description = "Project name in GCP that uptime check will be created in"
}

variable "region" {
    type = string
    description = "Project name in GCP that uptime check will be created in"
}

variable "env" {
    type = string
    description = "Project name in GCP that uptime check will be created in"
}



variable "uptime_check_name" {
    type = string
    description = "Uptime check display name"
}

variable "uptime_check_timeout" {
    type = string
    description = "Maximum amount of time to wait for the request to complete"
}

variable "uptime_check_period" {
    type = string
    description = "How often, in seconds, the uptime check is performed"
}

variable "uptime_check_url" {
    type = string
    description = "The path to the page to run the check against"
}

variable "uptime_check_port" {
    type = string
    description = "The port to the page to run the check against"
}

variable "uptime_check_request_method" {
    type = string
    description = "The HTTP request method to use for the check"
}

variable "uptime_check_host" {
    type = string
    description = "The host to run the check against"
}

variable "channel_name" {
    type = string
    description = "The display name for this notification channel"
}

variable "alert_policy_name" {
    type = string
    description = "A display name for this alert policy"
}

variable "condition_name" {
    type = string
    description = "A display name for condition"
}

variable "alignment_period" {
    type = string
    description = "The lookback period for each time series"
}

variable "per_series_aligner" {
    type = string
    description = "The approach to be used to align individual time series"
}

variable "cross_series_reducer" {
  type = string
  description = "The approach to be used to combine time series"
}

variable "comparison" {
  type = string
  description = "The comparison to apply between the time series and the threshold"
}

variable "duration" {
  type = string
  description = "The amount of time that a time series must violate the threshold to be considered failing"
}

variable "trigger_count" {
  type = string
  description = "The absolute number of time series that must fail the predicate for the condition to be triggered"
}

variable "threshold_value" {
  type = string
  description = "A value against which to compare the time series"
}

variable "combiner" {
  type = string
  description = "How to combine the results of multiple conditions to determine if an incident should be opened"
}





